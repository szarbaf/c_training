/*
This program is designed to convert number n in base 10 to a number in base b and save it as a string. Also, it right justifies the string in case it has length lower than MIN_LENGTH.
*/

#include <stdio.h>
#include <string.h>

#define MIN_LENGTH 5

void itob(int n, char *s, int b);

int main()
{
	int n = 15638, b = 12;
	char s[100];

	itob(n, s, b);

	printf("The original number was : %d and was converted to base %d and the result is : %s\n", n, b, s);
	
	return 0;
}

void itob(int n, char *s, int b)
{
	int num[100], d_n = n, i, j;

	
	for (i = 0; d_n >= b; i++)
	{
		num[i] = d_n % b;
		d_n /= b;
	}
	num[i] = d_n;

	int temp;
	for (int start = 0, end = i; start < end; start++ , end--)
	{
		temp = num[start];
		num[start] = num[end];
		num[end] = temp;
	}


	for (j = 0; j <= i; j++)
	{
		if ( num[j] > 9 )
			s[j] = 'a' + (num[j] - 10);
		else
			s[j] = '0' + num[j];
	}
	
	int diff = (MIN_LENGTH - 1) - i, 
	    length = j;
	if (diff)
	{
		int temp_s[100];
		
		for (int k = 0; k < diff; k++)
			temp_s[k] = '$';

		for (int k = 0; k <= j; k++)
			temp_s[k + diff] = s[k];
		
		length += diff;

		for(int k = 0; k <= length; k++)
			s[k] = temp_s[k];
	}

	s[length] = '\0';
		
}
