/*
This program converts tabs and newlines to \t and \n respectively. 
*/

#include <stdio.h>

int main()
{
	char dest[1000], source[1000] = "This is a sample test. Here comes a tab: \t and newline. :\n and here is a series of them : \t \t \t \n \n \t ";
	int j = 0;
	for (int i = 0; source[i] != '\0'; i++)
	{
		switch (source[i]){
			case '\t':
				dest[++j] = '\\';
			        dest[++j] = 't';
				break;
			case '\n':
				dest[++j] = '\\';
			        dest[++j] = 'n';
				break;
			default:
				dest[++j] = source[i];
		}
	}

	printf("The original string is : %s\n", source);
	printf("The converted string is : %s\n", dest);

	return 0;
		
}
