/*
This program compares the running time for the binary search of two different implementations of 
binary search. The list containing the values is in ascending order.
*/

#include <stdio.h>
#include <time.h>

#define ARRAY_SIZE 100000

int main()
{
	int list[ARRAY_SIZE];
	int target = 19, index;

	for (int i = 0; i < ARRAY_SIZE; i++)
		list[i] = 3 * i - 1;

	//First implementation(according to the book)

	int low = 0, high = ARRAY_SIZE - 1, mid, state = 1;
	clock_t tic, toc;

	tic = clock();
	index = -1;
	while (high >= low)
	{
		mid = (high + low) / 2;

		if (target < list[mid])
			high = mid - 1;
		else if (target > list[mid])
			low = mid + 1;
		else
		{
			index = mid;
			break;
		}
	}
	toc = clock();

	printf("The index for the matching value is : %5d\n", index);
	printf("Running time for the first implementation was : %.5f\n", (double)(toc - tic) / CLOCKS_PER_SEC);
	

	//Second implementation

	low = 0;
	high = ARRAY_SIZE - 1;

	tic = clock();
	index = -1;
	while (high > low)
	{
		mid = (high + low) / 2;

		if (target < list[mid])
			high = mid - 1;
		else
			low = mid;
	}

	if (list[low] == target)
		index = low;
	toc = clock();

	printf("The index for the matching value is : %5d\n", index);
	printf("Running time for the second implementation was %.5f\n", (double)(toc - tic) / CLOCKS_PER_SEC);

	system("PAUSE");

	return 0;

}