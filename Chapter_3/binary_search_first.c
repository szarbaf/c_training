#include <stdio.h>
#include <time.h>

#define LIST_SIZE 100000

int main()
{
	int list[LIST_SIZE], target = 20;

	for (int i = 0; i < LIST_SIZE; i++)
		list[i] = 3*i - 1;

	int low = 0, high = LIST_SIZE, mid, index = -1;
	clock_t tic, toc;

	tic = clock();
	while (high >= low)
	{
		mid = (high + low) / 2;

		if (list[mid] < target)
			low = mid + 1;
		else if (list[mid] > target)
			high = mid - 1;
		else
		{
			index = mid;
			break;
		}
	}
	toc = clock();

	printf("The index for the matching value is : %5d\n", index);
	printf("The running time for the first implementation is : %5.4f\n", 
			(double)(toc - tic) / CLOCKS_PER_SEC);

	return 0;
}
