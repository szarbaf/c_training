/*
This program expands strings such as a-z and 0-9 to abc...xyz and 012...789 .
*/

#include <stdio.h>
#include <math.h>

int main()
{
	char dest[1000], source[1000] = "Hello, this is a test : a-z 0-9 o-e 6-2 ad-p 58-3.";

	int increment, range;
	
	int j = 0;
	for (int i = 0; source[i] != '\0'; ++i)
	{
		if (source[i] == '-' && i >= 2)
		{
			j -= 1;
			if (source[i - 1] > source[i + 1])
				increment = -1;
			else
				increment = 1;
			range = abs(source[i - 1] - source[i + 1]);

			for (int k = 0; k <= range; ++k)
				dest[j++] = source[i - 1] + increment * k;
			i = i + 2;
		}
		else
			dest[j++] = source[i];
	}

	dest[j] = '\0';

	printf("The initial string was : %s\n", source);
	printf("The resulting string become : %s\n", dest);

	system("PAUSE");
	return 0;
}