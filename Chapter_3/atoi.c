/*
This program converts an integer to a string (The counterpart of atoi)
*/

#include <stdio.h>
#include <string.h>

void itoa(char* Asci, int n);

int main()
{
	int n = -125;
	char Asci[100];

	itoa(Asci, n);
	printf("The integer : %d\nThe string %s\n", n, Asci);

	system("PAUSE");
	return 0;
}

void itoa(char* Asci, int n)
{
	int negative = 1, j = 0;

	if (n >= 0)
		negative = 0;

	do{
		Asci[j++] = abs(n % 10) + '0';
	} while ((n /= 10) != 0);

	if (negative)
		Asci[j++] = '-';
	
	Asci[j] = '\0';
	_strrev(Asci);
}


