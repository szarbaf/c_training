/*
This program is designed to convert number n in base 10 to a string using a recursive function.
*/

#include <stdio.h>
#include <string.h>

#define MIN_LENGTH 5

void itoa(int n, char *s, int *place);

int main()
{
	int n = 15638;
	int place;
	char s[100];

	place = 0;

	itoa(n, s, &place);

	s[place] = '\0';

	printf("The original number was : %d and the result is : %s\n", n, s);

	system("PAUSE");
	return 0;
}

void itoa(int n, char *s, int *place)
{
	int remainder, quotient;

	quotient = n / 10;
	remainder = n % 10;

	if (quotient != 0)
	{
		itoa(quotient, s, place);
		s[*place] = remainder + '0';
	}
	else
	{
		s[0] = remainder + '0';
	}
	(*place)++;
}
