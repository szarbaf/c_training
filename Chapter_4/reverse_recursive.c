/*
This program reverse a given string using a recursive function.
*/

#include <stdio.h>
#include <string.h>

void reverse_r(char output[]);

int main()
{
	char output[100], input[] = "Shapal";
	strcpy_s(output, 100, input);
	
	reverse_r(output);

	printf("The original string was : %s and the reverse is : %s\n", input, output);

	system("PAUSE");
	return 0;
}

void reverse_r(char output[])
{
	int length;
	length = strlen(output);

	if (length > 1)
	{
		char tmp[100];
		int i;

		for (i = 1; output[i] != '\0'; i++)
			tmp[i - 1] = output[i];
		tmp[i - 1] = '\0';

		reverse_r(tmp);

		output[length - 1] = output[0];
		for (i = 0; tmp[i] != '\0'; i++)
			output[i] = tmp[i];
	}
}