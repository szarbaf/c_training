/*
This program is a calculator based on the Reverse Polish logic. For more info, see: 
p.74 of "The C Programming Language" by B. W. Kernichan 
*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <math.h>

#define NUMBER 0
#define IGNORE 2
#define MAXOP 100
#define MAXVAL 100
#define MAXBUFF 100

int getop(char s[]);
double pop(void);
void put(double num);

void rm_wspace(void);
void get_num(char s[], int i);

void show_stack(void);
void empty_stack(void);
void swap_stack(void);
int math_funcs(char s[]);

void set_var(void);
void put_var(char s[], int c);

int getch(void);
void ungetch(int num);

int main()
{

	int type;
	double op2;
	char s[MAXOP];

	while ((type = getop(s)) != EOF)
	{
		switch (type)
		{
		case NUMBER :
			put(atof(s));
			break;

		case '+' :
			put(pop() + pop());
			break;
		
		case '-' :
			op2 = pop();
			put(pop() - op2);
			break;
		
		case '*' :
			put(pop() * pop());
			break;
		
		case '/' :
			op2 = pop();
			if (op2 != 0)
				put(pop() / op2);
			else
			{
				printf("Error, zero division...\n");
				return 0;
			}
			break;

		case '%':
			op2 = pop();
			put( (int)pop() % (int)op2);
			break;

		case ';':
			printf("The final answer is : %f\n", pop());
			break;

		case IGNORE:
			break;
		
		default :
			printf("Error, Unknown parameter...\n");
			break;
		}
	}

	return 0;
}


int sp = 0;
double val[MAXVAL];

double pop(void)
{
	if (sp > 0)
		return val[--sp];
	else
	{
		printf("Error, stack array empty...\n");
		return 0.0;
	}
}

void put(double num)
{
	if (sp < MAXVAL)
		val[sp++] = num;
	else
		printf("Error, stack array full...\n");
}

int getop(char s[])
{
	int c;
	rm_wspace();
	s[0] = c = getch();

	s[1] = '\0';
	
	int i = 0, dummy;

	if (!isdigit(c) && c != '.')
	{
		if (c == '*' || c == '/' || c == '%' || c == ';')
			return c;
		else if (c == '\n')
			return IGNORE;
		else if (c == '+' || c == '-')
		{
			dummy = c;
			if (isdigit(c = getch()))
			{
				s[i++] = dummy;
			}
			else
				return dummy;
		}
		//Command mode
		else if (c == '\\')
		{
			c = getch();

			if (c == 'S' || c == 's')
				show_stack();

			else if (c == 'C' || c == 'c')
				empty_stack();

			else if (c == 'W' || c == 'w')
				swap_stack();

			else if (c == 'M' || c == 'm')
				return math_funcs(s);

			else if (c == 'V' || c == 'v')
				set_var();

			else
				printf("Error, invalid command : %c ...\n", c);

			return IGNORE;
		}
		else if ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z'))
		{
			put_var(s, c);
			return NUMBER;
		}
		else if (c == 'EOF')
			return 'EOF';
		else
		{
			printf("Error, invalid character : %c ...\n", c);
			return IGNORE;
		}
	}

	ungetch(c);
	get_num(s, i);
	
	return NUMBER;
}

void rm_wspace(void)
{
	int c;
	while ((c = getch()) == ' ' || c == '\t')
		;
	ungetch(c);
}

void get_num(char s[], int i)
{
	int c;
	rm_wspace();

	while (isdigit(c = getch()))
		s[i++] = c;

	if (c == '.')
	{
		s[i++] = '.';
		while (isdigit(c = getch()))
			s[i++] = c;
	}

	s[i] = '\0';

	if (c != EOF)
		ungetch(c);
}

void show_stack(void)
{
	if (sp > 0)
		printf("The top element of the stack is : %f\n", val[sp - 1]);
	else
			printf("The stack is empty...\n");
}

void empty_stack(void)
{
	if (sp > 0)
		sp = 0;
	else
		printf("The stack is already empty...\n");
}

void swap_stack(void)
{//Swap top two elements of the stack.
	
	double dummy;
	if (sp > 1)
	{
		dummy = val[sp - 2];
		val[sp - 2] = val[sp - 1];
		val[sp - 1] = dummy;
	}
	else
		printf("The stack has less than two elements...\n");
}

double var[27];

void set_var(void)
{
	int c, temp,i = 0;
	double val;
	char num[100];
	rm_wspace();

	if ((c = getch()) >= 'A' && c <= 'Z')
	{
		rm_wspace();
		if (( temp = getch() ) == '+' || temp == '-')
		{
			num[i++] = temp;
			temp = getch();
		}
		if (isdigit(temp))
		{
			ungetch(temp);
			get_num(num, i);
			val = atof(num);
			var[c - 'A'] = val;
			printf("%c = %f\n", c, val);
		}
		else
			printf("Error, the value for the variable is not a number...\n");
	}
	else if (c >= 'a' && c <= 'z')
	{
		rm_wspace();
		if (( temp = getch() ) == '+' || temp == '-')
		{
			num[i++] = temp;
			temp = getch();
		}
		if (isdigit(temp))
		{
			ungetch(temp);
			get_num(num, i);
			val = atof(num);
			var[c - 'a'] = val;
			printf("%c = %f\n", c, val);
		}
		else
			printf("Error, the value for the variable is not a number...\n");
	}
	else
		printf("Error, invalid character inserted...\n");
}

void put_var(char s[], int c)
{
	if (c >= 'A' && c <= 'Z')
		sprintf_s(s, 100, "%.4f", var[c - 'A']);
	else
		sprintf_s(s, 100, "%.4f", var[c - 'a']);
}

#define MAX_FUNC_LENGTH 100
#define MAX_ARG_LENGTH 100

int math_funcs(char s[])
{
	int c, counter = 0;

	char func[MAX_FUNC_LENGTH], argument[MAX_ARG_LENGTH];
	double num;

	rm_wspace();

	while ( (c = getch()) != '(' && (counter < MAX_FUNC_LENGTH) )
		func[counter++] = c;

	if (counter >= MAX_FUNC_LENGTH)
	{
		printf("The function name length is too long or not properly defined...\n");
		getch();
		return IGNORE;
	}

	func[counter] = '\0';

	get_num(argument, 0);
	num = atof(argument);

	if ( !( strcmp(func, "sin") && strcmp(func, "cos") && strcmp(func, "exp") && strcmp(func, "log") ) )
	{
		if (strcmp(func, "sin") == 0)
			num = sin(num);

		else if (strcmp(func, "cos") == 0)
			num = cos(num);

		else if (strcmp(func, "exp") == 0)
			num = exp(num);

		else
			num = log(num);

		sprintf_s(s, 100, "%.4f", num);

		getch();
		return NUMBER;
	}
	else
	{
		printf("Function %s not defined...\n", func);
		return IGNORE;
	}

}

int Buffer[MAXBUFF];
int bp = 0;

int getch(void)
{
	return (bp > 0) ? Buffer[--bp] : getchar();
}

void ungetch(int num)
{
	if (bp < MAXBUFF)
		Buffer[bp++] = num;
	else
		printf("Error, Buffer is full...\n");
}

void ungets(char p[])
{
	int i = 0;

	while (p[i] != '\0')
	{
		ungetch(p[i]);
		i++;
	}
}
