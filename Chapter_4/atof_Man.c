/*
This program converts a string of float number to a float number.
*/

#include <stdio.h>
#include <math.h>
#include <ctype.h>

void atof_man(char s[], float *num);

int main()
{
	char s[100] = "  -569.25e+2";
	float num;
	atof_man(s, &num);

	printf("The corresponding string was : %s and the number is %f\n", s, num);

	system("PAUSE");
	return 0;
}

void atof_man(char s[], float *num)
{
	*num = 0;

	int i;
	for (i = 0; isspace(s[i]); i++)
		;

	int sign = ((s[i] == '-') ? -1 : 1);
	if (s[i] == '-' || s[i] == '+')
		i++;

	for (; isdigit(s[i]); i++)
		*num = 10 * (*num) + (s[i] - '0');

	if (s[i] == '.')
	{
		i++;
		for (float power = 0.1; isdigit(s[i]); i++)
		{
			*num += power * (s[i] - '0');
			power /= 10;
		}
	}

	if (s[i] == 'e' || s[i] == 'E')
	{
		i++;
		int sign_p = ( (s[i] == '-') ? -1 : 1 );
		if (s[i] == '-' || s[i] == '+')
			i++;

		*num *= pow(10, sign_p * (s[i] - '0'));
	}

	*num *= sign;
}