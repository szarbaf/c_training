/*
This program is designed to use a macro swap(t,a,b) for swaping the value of two parameters a and b 
of type t;
*/

#include <stdio.h>

#define swap(t, a, b)\
	t tmp;\
	tmp = a;\
	a = b;\
	b = tmp;

int main()
{
	int a = 1, b = 2;

	printf("The values of a and b are : a = %d   b = %d\n", a, b);

	swap(int, a, b);

	printf("The values of a and b after the swap are : a = %d   b = %d\n", a, b);

	system("PAUSE");

	return 0;
}
