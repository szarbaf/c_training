/*
This program returns the index of the rightmost pattern match for t in s.
*/

#include <stdio.h>
#include <string.h>

int strindex(char s[], char t[]);

int main()
{
	char s[100] = "Hi! The pattern to be found is : shapal. So do the best shapal possible.";
	char t[100] = "shapal";

	int index = strindex(s, t);
	
	if (index)
	{
		printf("The matching pattern is at index %d as it reads : \n", index);

		for (int i = index; s[i] != '\0'; i++)
			putchar(s[i]);
		printf("\n");
	}
	else
		printf("The pattern %s doesn't match pattern %s\n", t, s);


	system("PAUSE");
	return 0;

}


int strindex(char s[], char t[])
{
	int Length = strlen(s);

	for (int i = Length - 1; i >= 0; i--)
	{
		
		if (s[i] == t[0])
		{
			int k = 1;
			for (int j = i + 1; t[k] != '\0' && s[j] != '\0' && s[j] == t[k]; j++, k++)
				;
			if (t[k] == '\0')
				return i;
		}
	}

	return -1;

}