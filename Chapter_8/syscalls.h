#define MAX_OPEN 20
#define MAX_BUFF 4096
#define MODE 0664

enum _flags {
	_READ = 01,
	_WRITE = 02,
	_UNBUF = 04,
	_EOF = 010,
	_ERR = 020
};

typedef struct my_fp {
	int cnt;
	char *ptr;
	char *base;
	int flag;
	int fd;

} my_FILE;

my_FILE _iob[MAX_OPEN];

my_FILE* my_fopen(char *fname, char *mode);
void my_fclose(my_FILE *fp);
void my_fputs(char *s, my_FILE *fp);
void my_fflush_out(my_FILE *fp);
void my_fputc(char c, my_FILE *fp);
int my_getc(my_FILE *fp);
void my_f_init(my_FILE *fp);
void my_fflush_in(my_FILE *fp);
void my_frand(int num, my_FILE* fp);
