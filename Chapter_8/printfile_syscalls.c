/*
This program prints files, each on a new page with its name and a counter while only using system calls instead of the standard library functions.
*/ 

#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#define BUFFSIZE 1000

void write_filename(char *filename, int counter);
void filecopy(int fd, int out);

int main(int argc, char *argv[])
{
	int fd;
	char error_open[50] = "Error opening file : ";

	int counter = 1;
	
	if (argc == 1)
		filecopy(0, 1);
	else
	{

		for (int i = 1 ; i < argc ; i++)
		{

			if ( (fd = open(argv[i], O_RDONLY, 0)) != -1  )
			{
				write_filename(argv[i], counter++);	
				filecopy(fd, 1);
			}
			else

			{
				write(2, error_open, sizeof error_open);
				write(2, argv[i], strlen(argv[i]) );
				write(2, "\n", 1);
			}
		}

	}
			
}

void write_filename(char *filename, int counter)
{
	char first_part[50] = "\n\n***************File name : ";
	char second_part[50] = " File count : ";
	char third_part[50] = " ***************\n\n";
	char counter_buff[10];
	sprintf(counter_buff, "%d", counter);

	write(1, first_part, sizeof first_part);
	write(1, filename, strlen(filename));
	write(1, second_part, sizeof second_part);
	write(1, counter_buff, strlen(counter_buff));
	write(1, third_part, sizeof third_part);

}

void filecopy(int fd, int out)
{
	
	char buf[BUFFSIZE];
	int read_stat, write_stat;
	char error_stdout[50] = "Error writing to stdout.\n";

	while ( ( read_stat = read(fd, buf, BUFFSIZE) ) > 0 )
	{
		write_stat = write(out, buf, read_stat);
		if (read_stat != write_stat)
			write(2, error_stdout, sizeof error_stdout);
	};
	write(out, "\f", 1);
}
