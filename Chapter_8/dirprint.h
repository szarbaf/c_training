#define MAX_NAME 14

struct Dir
{
	char name[MAX_NAME+1];
	int fd;
};

void my_fstat(char *name);

void dirwalk(char *name);

void stat_print(char *name, struct stat *stat_buf);

struct Dir *my_opendir(char *name);

struct dirent *my_readdir(struct Dir *dire);

void my_closedir(struct Dir *dire);

