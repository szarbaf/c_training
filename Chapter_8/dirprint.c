/*
This program prints the information for each file specified in the command line. For each directory entry, it recursively goes through it and prints the information for the files.
*/ 

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <string.h>
#include <fcntl.h>
#include "dirprint.h"

int main(int argc, char *argv[])
{
	
	if (argc == 1)
		my_fstat(".");
	else
		while (argc-- > 1)
			my_fstat(*++argv);

	return 0;

}

void my_fstat(char *name)
{	

	struct stat stat_buf;

	if (stat(name, &stat_buf) == -1)
	{
		fprintf(stderr, "my_fstat: Could not open file %s.\n", name);
		return;
	}

	stat_print(name, &stat_buf);
	if ( (stat_buf.st_mode & S_IFMT) == S_IFDIR)
		dirwalk(name);

}

void dirwalk(char *name)
{
	struct dirent *dire;
	struct Dir *dir;
	char fname[MAX_NAME];

	if ( (dir = my_opendir(name)) ==  NULL)
	{
		fprintf(stderr, "dirwalk: Cannot open directory : %s\n", name);
		return;
	}

	while ( (dire = my_readdir(dir)) != NULL )
	{
		if (strncmp(dire->d_name, ".", MAX_NAME-1) == 0 || strncmp(dire->d_name, "..", MAX_NAME-1) == 0 )
			continue;
		
		if (strlen(name) + strlen(dire->d_name) > MAX_NAME - 1)
		{
			fprintf(stderr, "dirwalk: The name %s/%s is too long.\n", name, dire->d_name);
		}
		
		sprintf(fname, "%s/%s", name, dire->d_name);
		my_fstat(fname);
	}
	my_closedir(dir);
}

void stat_print(char *name, struct stat *stat_buf)
{
	printf("name : %-14s\t size : %-8ld\t inode number : %-8ld\n", name, stat_buf->st_size, stat_buf->st_ino);
}

struct Dir *my_opendir(char *name)
{
	int fd;
	struct stat stat_buf;
	struct Dir *dp;
	if ( (fd=open(name, O_RDONLY, 0)) == -1  
			|| stat(name, &stat_buf) == -1
			|| (stat_buf.st_mode & S_IFMT ) != S_IFDIR 
			|| ( dp=calloc(1, sizeof(struct Dir)) ) == NULL
	   )
	{
		fprintf(stderr, "my_opendir: Failure opening directory %s.\n", name);
		return NULL;
	}
	dp->fd = fd;
	return dp;
}

struct dirent *my_readdir(struct Dir *dir)
{
	static struct dirent d;
	struct dirent d_buf;
	while ( read(dir->fd, &d_buf, sizeof(d_buf)) == sizeof(d_buf) )
	{
		if (d_buf.d_ino == 0)
			continue;

		d.d_ino = d_buf.d_ino;
		strncpy(d.d_name, d_buf.d_name, MAX_NAME);
		d.d_name[MAX_NAME] = '\0';

		return &d;
	}
	return NULL;
}

void my_closedir(struct Dir *dir)
{
	if(dir)
	{
		close(dir->fd);
		free(dir);
	}
	else
		fprintf(stderr, "my_closedir: Warning, the structure pointer is NULL.\n");
}
