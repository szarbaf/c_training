/*
This file is used for replicate of library functions such as fopen and fillbuf.
*/ 

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include "syscalls.h"


int main()
{
	
	clock_t start, end;
	
	char *fname = "test.txt";
	
	srand(time(NULL));

	start = clock();

	my_FILE *fp = my_fopen(fname, "w");
	my_fputs("Hello!\n", fp);
	my_fclose(fp);

	fp = my_fopen(fname, "a");
	my_fputs("Hello again!\n", fp);
	my_fclose(fp);

	fp = my_fopen(fname, "a");
	my_frand(100000, fp);
	my_fclose(fp);
	
	fp = my_fopen(fname, "r");
	/*
	int c;
	while ( ( c = my_getc(fp) ) != EOF )
		putc(c, stdout);
	*/
	my_fclose(fp);
	end = clock();

	printf("The time spent for the operation was : %f\n", (float)(end - start) / CLOCKS_PER_SEC);

	return 0;

}



my_FILE* my_fopen(char *fname, char *mode)
{
	if ( *mode != 'r' && *mode != 'w' && *mode != 'a' )
	{
		fprintf(stderr, "my_fopen: Invalid mode %c.\n", *mode);
		return NULL;
	}

	my_FILE *fp;
	int fd;
	
	for (fp = _iob; fp < _iob + MAX_OPEN; fp++)
		if ( (fp->flag & (_READ | _WRITE)) == 0 )
			break;
	if ( fp >= _iob + MAX_OPEN )
	{
		fprintf(stderr, "my_fopen: Too many opened files.\n");
		return NULL;
	}

	if (*mode == 'r')
		fd = open(fname, O_RDONLY, 0);

	else if (*mode == 'w')
		fd = creat(fname, MODE);
	else
	{
		if ( (fd = open(fname, O_WRONLY, 0)) < 0 )
				fd = creat(fname, MODE);
		lseek(fd, 0L, 2);
	}

	if (fd < 0)
	{
		fprintf(stderr, "my_fopen: Could not open file : %s.\n", fname);
		return NULL;

	}

	fp->fd = fd;
	fp->cnt = 0;
	fp->base = NULL;
	fp->flag = *mode == 'r' ? _READ : _WRITE;
	my_f_init(fp);
	return fp;
}

void my_fclose(my_FILE *fp)
{
	if ( (fp->flag & _WRITE) != 0 )
		my_fflush_out(fp);
	if ( (fp->flag & _READ) != 0 )
		my_fflush_in(fp);

	fp->flag = 0;

	close(fp->fd);
}

void my_fputs(char *s, my_FILE *fp)
{
	
	int length = strlen(s);
	if (s <= 0)
	{
		fprintf(stderr, "my_fputs: The length of the string is non-positive.\n");
		exit(EXIT_FAILURE);
	}
	
	if (fp->flag != _WRITE)
	{
		fprintf(stderr, "my_fputs: The file is not open for writing.\n");
		exit(EXIT_FAILURE);
	}

	while ( length-- > 0)
	{
		if (fp->cnt >= MAX_BUFF - 1)
			my_fflush_out(fp);	

		*(fp->ptr) = *s++;
		fp->ptr++;
		fp->cnt++;
	}

}

void my_fflush_out(my_FILE *fp)
{
	fp->ptr = '\0';
	
	if ( write(fp->fd, fp->base, fp->cnt) < fp->cnt )
		fprintf(stderr, "my_fflush_out: Error writing to the file.\n");

	fp->ptr = fp->base;
	fp->cnt = 0;
}

void my_fputc(char c, my_FILE *fp)
{

	if (fp->flag != _WRITE)
	{
		fprintf(stderr, "my_fputc: The file is not open for writing.\n");
		exit(EXIT_FAILURE);
	}


	if (fp->cnt >= MAX_BUFF - 1)
		my_fflush_out(fp);	

	*(fp->ptr) = c;
	fp->ptr++;

}

int my_getc(my_FILE *fp)
{

	if ( (fp->flag & _READ) != 1)
	{
		fprintf(stderr, "my_getc: The file is not open for reading.\n");
		exit(EXIT_FAILURE);
	}

	if (fp->cnt == 0)
		my_fflush_in(fp); 

	if ( (fp->flag & _EOF) != 0)
		return EOF;

	fp->cnt--;
	return *fp->ptr++;

}

void my_f_init(my_FILE *fp)
{
	fp->base = calloc(MAX_BUFF, sizeof(char));
	fp->ptr = fp->base;
}

void my_fflush_in(my_FILE *fp)
{
	int count;
	count = read(fp->fd, fp->base, MAX_BUFF);
	if (count == 0)
	{
		fp->flag = _EOF;
		return;
	}
	fp->cnt = count;
	fp->ptr = fp->base;
}

void my_frand(int num, my_FILE* fp)
{

	char c;

	if (fp->flag != _WRITE)
	{
		fprintf(stderr, "my_frand: The file is not open for writing.\n");
		exit(EXIT_FAILURE);
	}

	while ( num-- > 0)
	{
		if (fp->cnt >= MAX_BUFF - 1)
			my_fflush_out(fp);	

		c = (char) ( ('z' - 'A') * ( (float)rand() ) / RAND_MAX  + 'A');
			
		*(fp->ptr) = c;
		fp->ptr++;
		fp->cnt++;
	}

	*fp->ptr++ = '\n';
	fp->cnt++;


}
