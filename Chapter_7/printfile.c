/*
This program prints files, each on a new page with its name and a counter.
*/ 

#include <stdio.h>

int main(int argc, char *argv[])
{
	
	FILE *fp;
	int c;
	int counter = 1;
	for (int i = 1 ; i < argc ; i++)
		if ( (fp = fopen(argv[i], "r")) != NULL  )
		{
			printf("\n\n***************File name : %-20s\t File count : %d ***************\n\n", argv[i], counter++);
			while ( (c = getc(fp)) != EOF )
				putc(c, stdout);
			printf("\f");
		}
		else
			printf("COULD NOT OPEN %s.\n", argv[i]);
}
