/*
This program outputs the input in a sensible way, i.e. it outputs non-graphic characters in octal format (" \\%-03c ")
and breaks long lines.
*/ 

#include <stdio.h>
#include <ctype.h>

#define OCTAL_LENGTH 6
#define MAX_LINE 100

int length_checker(int pos, int n);

int main()
{
	int c, pos;
	
	while ((c = getchar()) != EOF)
		if (iscntrl(c) || c == ' ')
		{
			pos = length_checker(pos, OCTAL_LENGTH);
			printf(" \\%03o ", c);

			if (c == '\n')
			{
				pos = 0;
				putchar('\n');
			}

		}
		else
		{
			pos = length_checker(pos, 1);
			putchar(c);
		}

	return 0;
}

int length_checker(int pos, int n)
{
	if (pos + n < MAX_LINE)
		return pos + n;
	else
	{
		putchar('\n');
		return n;
	}
}
