/*
This program tries to replicate the way original printf function works.
*/ 

#include <stdio.h>
#include <stdarg.h>
#include <ctype.h>

#define MAXSTRING 100

void miniprintf(char *format, ...);


int main()
{

	char *format = "%2.3f shapalia! zibooam? %04d\n";
	double first = 2.25;
	int second = 25;

	miniprintf(format, first, second);

	return 0;

}

void miniprintf(char *format, ...)
{
	va_list ap;
	char c;

	char tmp_fmt[MAXSTRING];
	int count;
	
	int ival;
	double dval;
	char *sval;
	va_start(ap, format);

	for (char *p = format; *p; p++)
	{
		if (*p != '%')
		{
			putchar(*p);
			continue;
		}
		
		count = 0;
		tmp_fmt[count++] = '%';
		while( *++p != '\0' && !isalpha(*p) && count < MAXSTRING - 1)
			tmp_fmt[count++] = *p;
		if (*p != '\0')
			tmp_fmt[count++] = *p;
		tmp_fmt[count] = '\0';


		switch ( tmp_fmt[count-1] )
		{
			case 'f':
				dval = va_arg(ap, double);
				printf(tmp_fmt, dval);
				break;
			
			case 'd':
			case 'i':
				ival = va_arg(ap, int);
				printf(tmp_fmt, ival);
				break;

			case 's':
				sval = va_arg(ap, char *);
				printf(tmp_fmt, sval);
				break;

			default:
				printf(tmp_fmt);
				break;

			
		}
	}

	va_end(ap);
}
