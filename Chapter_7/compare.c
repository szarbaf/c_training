/*

This program compares two files and prints the first line they differ.

*/ 

#include <stdio.h>
#include <string.h>

#define MAXLINE 100
int main()
{
	char *file1 = "miniprintf.c";
	char *file2 = "miniprintf_changed.c";

	FILE *fp1 = fopen(file1, "r");
	FILE *fp2 = fopen(file2, "r");

	char line1[MAXLINE], line2[MAXLINE];

	while ( fgets(line1, MAXLINE, fp1) != NULL && fgets(line2, MAXLINE, fp2) != NULL )
	{
		if ( strncmp(line1, line2, MAXLINE) )
			{
				printf("The two files are not the same. The different lines are as follows : \n");
				printf("line1 : %s\nline2 : %s\n", line1, line2);

				return 0;
			}
	}

	fgets(line2, MAXLINE, fp2);
	if ( feof(fp1) && feof(fp2) )
		printf("The two files are the same!\n");
	else
		printf("Some error occured while reading two files or the files are not of the same length.\n");

	return 0;
}
