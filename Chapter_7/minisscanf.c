/*
This program tries to replicate the way original sscanf function works.
*/ 

#include <stdio.h>
#include <stdarg.h>
#include <ctype.h>
#include <string.h>

#define MAXSTRING 100

int minisscanf(char *string, char *format, ...);
char* whitespace_skip(char *s);

int main()
{

	char *format = "%4lf shapalia! %*s %4d\n";
	char *string = "2.25 shapalia! zibooyam? 25";
	double first;
	int second;

	minisscanf(string, format, &first, &second);

	printf("The values for first and second are : %f\t%d\n", first, second);

	return 0;

}

int minisscanf(char *string, char *format, ...)
{
	va_list ap;

	char tmp_fmt[MAXSTRING], tmp_st[MAXSTRING];
	int count_fmt, count_st;

	int match_found = 0;
	
	int *ip;
	double *dp;
	char *sp;
	va_start(ap, format);
	
	for (char *p = format, *s = string; *p && *s; p++, s++)
	{
		p = whitespace_skip(p);
		s = whitespace_skip(s);
		if (*p != '%' && *p != '\0' && *s != '\0')
		{
			if (*s == *p)
			{
				continue;
			}
			else
			{
				va_end(ap);
				return match_found;
			}
		}

		if ( *s == '\0' || *p == '\0')
		{
			va_end(ap);
			return match_found;
		}
		
		count_fmt = 0;
		tmp_fmt[count_fmt++] = '%';
		while( *++p != '\0' && !isalpha(*p) && count_fmt < MAXSTRING - 1)
			tmp_fmt[count_fmt++] = *p;
		if (*p != '\0')
			tmp_fmt[count_fmt++] = *p;
		
		if ( tmp_fmt[count_fmt - 1] == 'l' && count_fmt < MAXSTRING - 1)
			tmp_fmt[count_fmt++] = *++p;

		tmp_fmt[count_fmt] = '\0';
		

		count_st = 0;
		tmp_st[count_st++] = *s;
		while ( !isspace(*++s) &&  *s != '\0' && count_st < MAXSTRING - 1 )
			tmp_st[count_st++] = *s;
		if (*s != '\0' && !isspace(*s))
			tmp_st[count_st++] = *s;
		tmp_st[count_st] = '\0';

		switch ( tmp_fmt[count_fmt-1] )
		{
			case 'f':
				dp = va_arg(ap, double*);
				match_found += sscanf(tmp_st, tmp_fmt, dp);
				break;
			
			case 'd':
			case 'i':
				ip = va_arg(ap, int*);
				match_found += sscanf(tmp_st, tmp_fmt, ip);
				break;

			case 's':
				sp = va_arg(ap, char *);
				match_found += sscanf(tmp_st, tmp_fmt, sp);
				break;

			default:
				if ( !strncmp(tmp_st, tmp_fmt, MAXSTRING - 1) )
					break;
				else
				{
					va_end(ap);
					return match_found;
				}
		}
	}

	va_end(ap);
	return match_found;
}

char* whitespace_skip(char *s)
{
	while ( isspace(*s) && *s != '\0')
		s++;
	return s;
}
