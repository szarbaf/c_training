#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

int main(int argc, char *argv[])
{
	int (*func)( int );
	int name_length = strlen(argv[0]);
	char *name = argv[0];
	if (name_length >= strlen("upper_to_lower") )
		name = argv[0] + name_length - strlen("upper_to_lower");

	if ( !strncmp(name, "upper_to_lower", 20) )
		func = tolower;
	else if ( !strncmp(name, "lower_to_upper", 20) )
		func = toupper;
	else
	{
		printf("Invalid function name : %s. The function name should be either \"upper_to_lower\" or \"lower_to_upper\"\n", name);
		exit(EXIT_FAILURE);
	}

	char c;
	while ( (c = getchar()) != EOF)
	{
		c = (*func)(c);
		printf("%c", c);
	}

	return 0;
}
