/*

This file accepts as the input some file names and prints the lines from each file that contain the pattern. if no arguments are given, it finds patterns from stdin.

*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAXLINE 100

#define KNRM  "\x1B[0m"
#define KMAG  "\x1B[35m"
#define KRED  "\x1B[31m"

void fpattern(FILE *fp, char *pattern, char *filename);

int main(int argc, char *argv[])
{

	char *pattern = "int";
	if (argc == 1)
		fpattern(stdin, pattern, "stdin");
	else
	{
		FILE *fp;
		for (int i = 1; i < argc; i++)
		{
			if ( ( fp = fopen(argv[i], "r") ) != NULL)
				fpattern(fp, pattern, argv[i]);
			else
			{
				printf("Error opening file : %s.\n", argv[i]);
				exit(EXIT_FAILURE);
			}
		}

	}

	return 0;

}

void fpattern(FILE *fp, char *pattern, char *filename)
{
	char line[MAXLINE], *pattern_start;
	char *line_tmp1, *line_tmp2;
	char *tmp1 = line_tmp1 = calloc(MAXLINE, sizeof(char));
	char *tmp2 = line_tmp2 = calloc(MAXLINE, sizeof(char));

	while ( fgets(line, MAXLINE, fp) != NULL )	
	{
		if ( (pattern_start = strstr(line, pattern)) != NULL )
		{
			line_tmp1 = tmp1;
			line_tmp2 = tmp2;
			strncpy(line_tmp1, line, MAXLINE);
			strncpy(line_tmp2, pattern_start, MAXLINE);

			*( line_tmp1 + (pattern_start - line) ) = '\0';
			line_tmp2 = line_tmp2 + strlen(pattern);

			printf(KMAG "%s : " KNRM  "%s" KRED "%s" KNRM "%s\n", filename, line_tmp1, pattern, line_tmp2);
			//printf()
		}

	}

	free(tmp1);
	free(tmp2);
}
