#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main()
{
	printf("Please enter a hexadecimal charachter.");
	char h[100];
	scanf_s("%s", h, 100);
	
	int start = 0;
	if (h[0] == '0')
	{
		if (h[1] == 'x' || h[1] == 'X')
			start = 2;
	}

	int length = 0;
	while (h[length] != '\0')
		length += 1;

	int sum = 0, digit,p,c;
	for (int i = start; i < length; i++)
	{
		if (h[i] >= 'a' && h[i] <= 'f')
			digit = h[i] - 'a' + 10;
		else if (h[i] >= 'A' && h[i] <= 'F')
			digit = h[i] - 'A' + 10;
		else if (h[i] >= '0' && h[i] <= '9')
			digit = h[i] - '0';
		else
		{
			printf("Invalid digit %c entered, exiting.\n", h[i]);
			system("PAUSE");
			exit(EXIT_FAILURE);
		}

		p = length - (i + 1);
		sum += pow(16, p) * digit;

	}

	printf("\nThe number %s in hexadecimal is equal to %d in decimal.\n", h, sum);
	system("PAUSE");

	return 0;
}