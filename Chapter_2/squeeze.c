/*
This is a program that accepts two strings and removes all the characters from the first string
that exist in the second one.
*/
#include <stdio.h>

int main()
{
	char s1[100], s2[100];

	printf("Please enter the first string to be scanned.\n");
	scanf_s("%s", s1, 100);
	printf("Please enter the second string for removing the chars from the first one.\n");
	scanf_s("%s", s2, 100);

	int c,j;
	for (int k = 0; s2[k] != '\0'; k++)
	{
		c = s2[k];

		j = 0;
		for (int i = 0; s1[i] != '\0'; i++)
		{
			if (s1[i] != c)
				s1[j++] = s1[i];
		}
		s1[j] = '\0';
	}
	printf("The new string is : %s .\n", s1);
	system("PAUSE");

	return 0;
}