#define _CRT_SECURE_NO_DEPRECATE

#include <stdio.h>
#include <string.h>
#include <limits.h>


#define OUT_COMMENT 0
#define IN_COMMENT 1
#define IN_MLINE_COMMENT 2
#define IN_SLINE_COMMENT 3

int c_char(int *c, char *ch, FILE *input);

int main()
{
	int c;
	int state = 0;

	char input_file[100], output_file[100], path[100];
	
	strcpy(path, "D:/C_Codes/Project1/Project1/");
	strcpy(input_file, path);
	strcpy(output_file, path);
	strcat_s(input_file, 100, "commented.txt");
	strcat_s(output_file, 100, "uncommented.txt");
	
	FILE* input = fopen(input_file,"r");
	FILE* output = fopen(output_file, "w");

	int check = 0;
	char ch;

	while ((c_char(&c, &ch, input)) != EOF)
	{
		if (c == '/')
		{
			state = IN_COMMENT;
			c_char(&c, &ch, input);

			if (c == '*')
			{
				check = 0;
				while ((c_char(&c, &ch, input)) != EOF && state == IN_COMMENT)
				{
					if (c == '*')
					{
						c_char(&c, &ch, input);
						if (c == '/'){
							state = OUT_COMMENT;
						}
						else
						{
							fputc('*', output);
							fputc(c, output);
						}
					}
					else
						fputc(c, output);
				}
				if (c != EOF)
					fputc(c, output);

			}


			else if (c != EOF && c != '/')
			{
				fputc('/', output);
				fputc(c, output);
			}
		}
		else
			fputc(c, output);
	}
	int kk = 123;
	printf("shapal %c \v mapal %d",'\a', kk + 1);
	c = getchar();
	return 0;
}

int c_char(int *c, char *ch, FILE *input)
{
	*c = fgetc(input);
	*ch = (char)*c;

	return *c;
}