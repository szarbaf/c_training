#include <stdio.h>

#define tab_size 4

int main()
{
	int c;
	while ((c = getchar()) != EOF)
	{
		if (c == '\t')
		{
			for (int i = 0; i < tab_size; i++)
				printf(" ");
		}
		else
			putchar(c);
	}
	return 0;

}