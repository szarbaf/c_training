/*
This program makes every character lower case in a given string.
*/

#include <stdio.h>

int main()
{
	char inp[100];
	
	printf("The size of char is : %d\n", sizeof(char));
	printf("Please enter a string to be lower cased.\n");
	scanf_s("%s", inp, 100);

	for (int i = 0; inp[i] != '\0'; i++)
		inp[i] = (inp[i] >= 'a' && inp[i] <= 'z') ? inp[i] : (inp[i] + 'a' - 'A');

	printf("The lower cased string is %s .\n", inp);
	system("PAUSE");

	return 0;

}