/*
This program counts the number of ones in a binary number.
*/
#include <stdio.h>

int main()
{
	int num, i;
	printf("Please enter a number for counting the number of ones in binary base.\n");
	scanf_s("%d", &num, sizeof(int));

	for (i = 0; num != 0; num &= (num - 1))
		i += 1;

	printf("The number of ones is : %d.\n", i);
	system("PAUSE");

	return 0;
}