#include <stdio.h>

#define IN_WORD 1
#define OUT_WORD 0
#define tab_size 6

int main()
{
	int c, counter = 0;
	int state = IN_WORD;
	int no_tabs, no_spaces;

	
	while ((c = getchar()) != EOF)
	{
		if (c == ' ')
		{
			if (state == OUT_WORD)
				counter += 1;
			else
			{
				state = OUT_WORD;
				counter = 1;
			}

		}

		else
		{
			if (state == OUT_WORD)
			{
				no_tabs = counter / tab_size;
				no_spaces = counter % tab_size;

				for (int i = 0; i < no_tabs; i++)
					printf("\t");
				for (int i = 0; i < no_spaces; i++)
					printf(" ");
				state = IN_WORD;
			}
			putchar(c);

		}

	}
		return 0;
}