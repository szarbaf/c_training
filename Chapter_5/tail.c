/*
This program prints the last n lines of a file where the filename is passed as a command line argument.
The default value of n is 10.
*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#define MAX_NUM_LINES 10
#define MAX_LENGTH 100

void fgetline(FILE *input, char *lines[], int *no_lines_p, int max_num);
int my_getline(char **line_p, FILE *input);

int main(int argc, char *argv[])
{
	int n = 10;

	while (argc > 0)
	{
		
		if ( (*++argv)[0] == '-' )
		{
			
			if (*++argv[0] == 'n')
			{

				if ( isdigit( (*++argv)[0] ) )
				{
					n = atoi(argv[0]);
					argc = 0;
				}

			}


		}
		argc--;

	}
	
	FILE *input;
	input = fopen("in.txt","r");
	
	char *lines[100];
	int counter = 0, no_lines;
	fgetline(input, lines, &no_lines, MAX_NUM_LINES);
	
	n = (n < no_lines) ? n : no_lines;
	printf("The last %d lines are : \n\n", n);
	
	for (int i = 1; i <= n; i++)
		printf("%s\n", lines[no_lines - i]);
		
		
	return 0;

}

void fgetline(FILE *input, char *lines[], int *no_lines_p, int max_num)
{
	*no_lines_p = 0;
	
	while ((*no_lines_p) < max_num && my_getline(lines + *no_lines_p, input) != EOF )
		(*no_lines_p)++;
}

int my_getline(char **line_p, FILE *input)
{
	int c, counter = 0;
	*line_p = malloc(MAX_LENGTH);
	
	while ( counter < MAX_LENGTH - 1 && (c = fgetc(input)) != EOF && c != '\n' )
		(*line_p)[counter++] = c;
	
	if (c != EOF && counter > 0)
		{
			(*line_p)[counter] = '\0';
			return counter;
		}
	else
		return EOF;
}



