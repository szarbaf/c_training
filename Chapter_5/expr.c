/*
This program is a Reverse Polish calculator which reads inputs
from the command line.
*/

#include <stdio.h>

#define MAX_CMD 100

int line_analyzer(char line[]);

int main(int argc, char *argv[])
{
	char command[MAX_CMD];
	int counter = 0;

	while (--argc)
	{
		command[counter++] = (*++argv)[0];
		command[counter++] = ' ';
	}
	command[counter++] = '\n';
	command[counter] = '\0';

	line_analyzer(command);

	return 0;
}
