/*
This program sorts a set of lines either lexicographically or numerically using function pointer
for determining the mode of sorting.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define MAXLINES 500
#define LINE_MAX_LENGTH 100

#define NUMERIC 1
#define REVERSE 2
#define FOLD 4

int readlines(char *lineptr[], int Max_Lines);
void writelines(char *lineptr[], int nlines);

void my_qsort(void *v[], int start, int end, int (*cmp)(void *, void *), int option);

void convert_upper(char *v[], int nlines);

int numcmp(char v1[], char v2[]);

int main(int argc, char *argv[])
{
	int nlines; 
	int option = 0;
	char *lineptr[MAXLINES];
	
	while (--argc >= 0)
	{
		//Numeric Comparison
		if ( strcmp(argv[argc], "-n") == 0 )
			option |= NUMERIC;
		
		//Reverse Sorting	
		else if ( strcmp(argv[argc], "-r") == 0)
			option |= REVERSE;
			
		//Fold Sorting(Not distinguishing between upper and lower case)	
		else if ( strcmp(argv[argc], "-f") == 0)
			option |= FOLD;
	}
	
	if ( (nlines = readlines(lineptr, 5)) > 0)
	{
		if (option & FOLD)
			convert_upper(lineptr, nlines);
			
		my_qsort((void **) lineptr, 0, nlines - 1, ( int(*)(void *, void *) ) (option&NUMERIC ? numcmp : strcmp), option );
		
		printf("The ordered lines are : \n");
		writelines(lineptr, nlines);
	}
	else
		printf("Reading lines not successful...\n");
		
	return 0;
}

int my_getline(char **line_p, FILE *input)
{
	int c, counter = 0;
	*line_p = malloc(LINE_MAX_LENGTH);
	
	while ( counter < LINE_MAX_LENGTH - 1 && (c = fgetc(input)) != EOF && c != '\n' )
		(*line_p)[counter++] = c;
	
	if (c != EOF && counter > 0)
		{
			(*line_p)[counter] = '\0';
			return counter;
		}
	else
		return EOF;
}

int readlines(char *lineptr[], int Max_Lines)
{
	int counter = 0;
	
	while ( my_getline(lineptr + counter++, stdin) && counter < Max_Lines)
	;
	
	return counter;
}

void writelines(char *lineptr[], int nlines)
{
	for (int i = 0; i < nlines; i++)
		puts(lineptr[i]);
}

int numcmp(char v1[], char v2[])
{
	double n1 = atof(v1);
	double n2 = atof(v2);
	
	if (n1 > n2)
		return 1;
	else if (n1 < n2)
		return -1;
	else
		return 0;
}

void swap(void *v[], int k1, int k2)
{
	void *tmp;
	
	tmp = v[k1];
	v[k1] = v[k2];
	v[k2] = tmp;
}

void my_qsort(void *v[], int start, int end, int (*cmp)(void *, void *), int option)
{
	int last = start;
	int reverse = option&REVERSE ? -1 : 1;
	
	if (start >= end)
		return;
	
	swap(v, start, (start + end) / 2);
	
	for (int i = start+1; i <= end; i++)
	{
		if ( (*cmp)(v[start], v[i]) * reverse > 0)
			swap(v, i, ++last);
	}
	
	swap(v, last, start);
	
	my_qsort(v, start, last - 1, cmp, reverse);
	my_qsort(v, last + 1, end, cmp, reverse);
}

void convert_upper(char *v[], int nlines)
{
	for (int i = 0; i < nlines; i++)
		v[i][0] = toupper( v[i][0] );
}








