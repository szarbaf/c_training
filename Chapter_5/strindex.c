/*
This program returns the first matching index of pattern string t in string s using pointer arithmetics.
*/

#include <stdio.h>

int strindex_p(char *s, char *t);

int main()
{
	char *s = "Shapal nago bala bego shompal...";
	char *t = "bala";

	int index = strindex_p(s, t);

	if (index)
		printf("The starting index of the matching pattern is : %d where it reads : %s\n", index, s + index);
	else
		printf("The pattern was not found.\n");

	system("PAUSE");

	return 0;
}

int strindex_p(char *s, char *t)
{
	int length_t = 0, length_s = 0;
	while (*t++ != '\0')
		length_t++;
	t -= 2;

	while (*s++ != '\0')
		length_s++;
	s -= 2;

	while (length_s--)
	{
		int i = 0;
		while (i < length_t && i < length_s && *(s - i) == *(t - i))
			i++;

		if (i == length_t )
			return (length_s - i + 1);

		s--;
	}

	return -1;
		
}