/*
This program converts month and day of year to the total day of the year.
*/

#include <stdio.h>

static char daytab[2][6] = {
	{0, 31, 28, 30, 31, 30},
	{0, 31, 29, 31, 30, 31}
};


int day_of_year(int year, int month, int day);

int main()
{
	int year = 1990, month = 2, day = 9, Tday;
	
	Tday = day_of_year(year, month, day);
	
	if (Tday > 0)
		printf("The total days for yyyy/mm/dd : %d/%d/%d is : %d\n", year, month, day, Tday);
	else
		printf("Invalid data...\n");
	
	return 0;
}

int day_of_year(int year, int month, int day)
{
	int leap = year%4 == 0 && year%100 != 0 || year%400 == 0;
	
	if (day > 31 || month > 12)
		return -1;
		
	for (int i = 0; i < month; i++)
		day += daytab[leap][i];
		
	return day;
}





