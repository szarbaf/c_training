/*
This program is a calculator based on the Reverse Polish logic. For more info, see:
p.74 of "The C Programming Language" by B. W. Kernichan
*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <math.h>

#define NORMAL 0
#define ERROR -1
#define NUMBER 0
#define IGNORE 2
#define MAXOP 100
#define MAXVAL 100

int line_analyzer(char line[]);
int getop(char s[], char line[], int *lp);
double pop(void);
void put(double num);

void rm_wspace(char line[], int *lp);
void get_num(char s[], int i, char line[], int *lp);

void show_stack(void);
void empty_stack(void);
void swap_stack(void);
int math_funcs(char s[], char line[], int *lp);

void set_var(char line[], int *lp);
void put_var(char s[], int c);

int sp = 0;
double val[MAXVAL];

double pop(void)
{
	if (sp > 0)
		return val[--sp];
	else
	{
		printf("Error, stack array empty...\n");
		return 0.0;
	}
}

void put(double num)
{
	if (sp < MAXVAL)
		val[sp++] = num;
	else
		printf("Error, stack array full...\n");
}

int line_analyzer(char line[])
{
	int type;
	double op2;
	char s[MAXOP];
	int lp = 0;

	while ((type = getop(s, line, &lp)) != '\n')
	{
		switch (type)
		{
		case NUMBER:
			put(atof(s));
			break;

		case '+':
			put(pop() + pop());
			break;

		case '-':
			op2 = pop();
			put(pop() - op2);
			break;

		case '*':
			put(pop() * pop());
			break;

		case '/':
			op2 = pop();
			if (op2 != 0)
				put(pop() / op2);
			else
			{
				printf("Error, zero division...\n");
				return ERROR;
			}
			break;

		case '%':
			op2 = pop();
			put((int)pop() % (int)op2);
			break;

		case ';':
			printf("The final answer is : %f\n", pop());
			break;

		case IGNORE:
			break;

		default:
			printf("Error, Unknown parameter...\n");
			break;
		}
	}
	return NORMAL;
}

int getop(char s[], char line[], int *lp)
{
	
	int c;
	rm_wspace(line, lp);
	s[0] = c = line[*lp];

	s[1] = '\0';

	int i = 0, dummy;

	if (!isdigit(c) && c != '.')
	{
		if (c == '*' || c == '/' || c == '%' || c == ';')
		{
			(*lp)++;
			return c;
		}
		else if (c == '\n')
			return '\n';
		else if (c == '+' || c == '-')
		{
			dummy = c;
			if (isdigit( line[(*lp)++] ))
			{
				s[i++] = dummy;
			}
			else
				return dummy;
		}
		//Command mode
		else if (c == '\\')
		{
			c = line[++(*lp)];
			(*lp)++;

			if (c == 'S' || c == 's')
				show_stack();

			else if (c == 'C' || c == 'c')
				empty_stack();

			else if (c == 'W' || c == 'w')
				swap_stack();

			else if (c == 'M' || c == 'm')
				return math_funcs(s, line, lp);

			else if (c == 'V' || c == 'v')
				set_var(line, lp);

			else
				printf("Error, invalid command : %c ...\n", c);

			return IGNORE;
		}
		else if ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z'))
		{
			put_var(s, c);
			(*lp)++;
			return NUMBER;
		}
		else
		{
			printf("Error, invalid character : %c ...\n", c);
			return IGNORE;
		}
	}

	get_num(s, i, line, lp);

	return NUMBER;
}

void rm_wspace(char line[], int *lp)
{
	int i = *lp;
	while (line[i] == ' ' || line[i] == '\t')
		i++;
	*lp = i;
}

void get_num(char s[], int i, char line[], int *lp)
{
	int c, j;
	rm_wspace(line, lp);

	j = *lp;

	while (isdigit(line[j]))
	{
		s[i] = line[j];
		j++;
		i++;
	}

	if (line[j] == '.')
	{
		s[i++] = '.';
		j++;
		while (isdigit(line[j]))
		{
			s[i] = line[j];
			j++;
			i++;
		}
	}

	s[i] = '\0';
	*lp = j;
}

void show_stack(void)
{
	if (sp > 0)
		printf("The top element of the stack is : %f\n", val[sp - 1]);
	else
		printf("The stack is empty...\n");
}

void empty_stack(void)
{
	if (sp > 0)
		sp = 0;
	else
		printf("The stack is already empty...\n");
}

void swap_stack(void)
{//Swap top two elements of the stack.

	double dummy;
	if (sp > 1)
	{
		dummy = val[sp - 2];
		val[sp - 2] = val[sp - 1];
		val[sp - 1] = dummy;
	}
	else
		printf("The stack has less than two elements...\n");
}

double var[27];

void set_var(char line[], int *lp)
{
	int c, temp, i = 0, j = *lp;
	double val;
	char num[100];
	rm_wspace(line, &j);

	c = line[j++];
	if (c >= 'A' && c <= 'Z')
	{
		rm_wspace(line, &j);
		if ((temp = line[j]) == '+' || temp == '-')
		{
			num[i++] = temp;
			temp = line[++j];
		}
		if (isdigit(temp))
		{
			get_num(num, i, line, &j);
			val = atof(num);
			var[c - 'A'] = val;
			printf("%c = %f\n", c, val);
		}
		else
			printf("Error, the value for the variable is not a number...\n");
	}
	else if (c >= 'a' && c <= 'z')
	{
		rm_wspace(line, &j);
		if ((temp = line[j]) == '+' || temp == '-')
		{
			num[i++] = temp;
			temp = line[++j];
		}
		if (isdigit(temp))
		{
			get_num(num, i, line, &j);
			val = atof(num);
			var[c - 'a'] = val;
			printf("%c = %f\n", c, val);
		}
		else
			printf("Error, the value for the variable is not a number...\n");
	}
	else
		printf("Error, invalid character inserted...\n");

	*lp = j;
}

void put_var(char s[], int c)
{
	if (c >= 'A' && c <= 'Z')
		snprintf(s, 100, "%.4f", var[c - 'A']);
	else
		snprintf(s, 100, "%.4f", var[c - 'a']);
}

#define MAX_FUNC_LENGTH 100
#define MAX_ARG_LENGTH 100

int math_funcs(char s[], char line[], int *lp)
{
	int c, counter = 0, i = *lp;

	char func[MAX_FUNC_LENGTH], argument[MAX_ARG_LENGTH];
	double num;

	rm_wspace(line, &i);

	while (line[i] != '(' && (counter < MAX_FUNC_LENGTH))
	{
		func[counter++] = line[i];
		i++;
	}
	i++;
	if (counter >= MAX_FUNC_LENGTH)
	{
		printf("The function name length is too long or not properly defined...\n");
		while (line[i] != ')' && line[i] != '\n')
			i++;
		*lp = ++i;
		return IGNORE;
	}

	func[counter] = '\0';

	get_num(argument, 0, line, &i);
	num = atof(argument);

	if (!(strcmp(func, "sin") && strcmp(func, "cos") && strcmp(func, "exp") && strcmp(func, "log")))
	{
		if (strcmp(func, "sin") == 0)
			num = sin(num);

		else if (strcmp(func, "cos") == 0)
			num = cos(num);

		else if (strcmp(func, "exp") == 0)
			num = exp(num);

		else
			num = log(num);

		snprintf(s, 100, "%.4f", num);

		*lp = i + 1;
		return NUMBER;
	}
	else
	{
		printf("Function %s not defined...\n", func);
		*lp = i + 1;
		return IGNORE;
	}

}

