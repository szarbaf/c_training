/*
This program extracts any free style float number from a stdin. This program stops as soon as it gets 
only whitespace as the input.
*/
#include <stdio.h>
#include <ctype.h>
#include <math.h>



int getfloat(char *line, int *np, int *lp);
int getline(char *line, int offset);

float num[100];

int main()
{
	char line[100];
	int lp, np, checker, c;
	
	np = 0;
	do
	{
		lp = 0;
		checker = getline(line, 100);	
		while ( getfloat(line, &np, &lp) != '\0')
			;

	} while (checker > 0);

	printf("The numbers that you entered are : \n");

	for (int i = 0; i < np; i++)
		printf("%.3f\t", num[i]);
	printf("\n");

	system("PAUSE");
	return 0;
}

int getfloat(char *line, int *np, int *lp)
{
	while (line[*lp] == ' ')
		(*lp)++;

	if (!isdigit(line[*lp]) && line[*lp] != '+' && line[*lp] != '-' && line[*lp] != '.')
	{
		(*lp)++;
		return line[*lp - 1];
	}

	int sign = 1;
	float number = 0;

	if (line[*lp] == '+' || line[*lp] == '-')
	{
		if (!isdigit(line[*lp + 1]))
		{
			(*lp)++;
			return line[*lp];
		}
		else
		{
			sign = (line[*lp] == '-' ? -1 : 1);
			(*lp)++;
		}
	}

	while ( isdigit(line[*lp]) )
	{
		number = 10 * number + (line[*lp] - '0');
		(*lp)++;
	}

	if (line[*lp] == '.')
	{
		if (isdigit(line[++(*lp)]))
		{
			int counter = 0;
			while (isdigit(line[*lp]))
			{
				number = 10 * number + (line[*lp] - '0');
				(*lp)++;
				counter++;
			}
			number /= pow(10, counter);
		}
		else
			return line[*lp];
	}

	num[*np] = number * sign;
	(*np)++;

	return 0;

}

int getline(char *line, int offset)
{
	int length = 0;
	while ( (line[length] = getc(stdin)) != EOF && line[length] != '\n' && length < offset - 1)
		length++;

	if (line[length] != EOF && length < offset - 1)
	{
		line[length] = '\0';
		return length;
	}
	else
		return -1;
}