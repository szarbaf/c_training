/*
This program converts an integer to a string using pointer arithmetics.
*/

#include <stdio.h>

void itoa_p(char *num_ch, int num);

int main()
{
	int num = -2458, checker;
	char *num_ch;

	num_ch = malloc(100 * sizeof(char));

	itoa_p(num_ch, num);

	printf("The number in string format is : %s\n", num_ch);

	free(num_ch);

	system("PAUSE");

	return 0;
}

void itoa_p(char *num_ch, int num)
{
	int quotient, remainder, counter = 0;
	char *tmp = malloc(100);
	if (num < 0)
	{
		*num_ch++ = '-';
		num *= -1;
	}
	else
		*num_ch++ = '+';

	quotient = num;
	
	do 
	{
		
		remainder = quotient % 10;

		*tmp++ = remainder + '0';

		quotient /= 10;
		counter++;

	} while (quotient);

	tmp--;

	for (; counter > 0; counter--)
		*num_ch++ = *tmp--;

	*num_ch = '\0';

	free(++tmp);
}