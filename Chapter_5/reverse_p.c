/*
This program converts a string using pointer arithmetics.
*/

#include <stdio.h>

#define MAX_LENGTH 100

void reverse_p(char *s, char *s_r);

int main()
{
	char *s = "Shapalia!";
	char *s_r = malloc(MAX_LENGTH);

	reverse_p(s, s_r);

	printf("The reverse string is : %s\n", s_r);

	free(s_r);

	system("PAUSE");

	return 0;
}

void reverse_p(char *s, char *s_r)
{
	int counter = 0, counter_p = 0;

	while (*++s != '\0')
		counter_p++;

	while (counter++ < MAX_LENGTH - 1 && counter_p-- >= 0)
		*s_r++ = *--s;

	*s_r = '\0';
}