/*
This program converts a the first encountered integer in a string to an integer value using pointer arithmetics.
*/

#include <stdio.h>

int atoi_p(char s[], int *num);

int main()
{
	char s[] = "Shapalia!-2584";

	int num, checker;

	checker = atoi_p(s, &num);

	if (checker == 1)
		printf("The first integer in the string is : %d\n", num);
	else
		printf("No integer number could be detected.\n");

	system("PAUSE");
	return 0;
}

int atoi_p(char s[], int *num)
{
	int test = 0;

	while (!isdigit(*s) && *s != '+' && *s != '-' && *s != EOF && *s != '\n')
		*s++;

	if (*s == EOF || *s == '\n')
		return 0;

	int sign = 1;
	if (*s == '+' || *s == '-')
	{
		if (isdigit(*++s))
			sign = (s[-1] == '-') ? -1 : 1;
		else
		{
			while (!isdigit(*s++))
				;
		}
		
	}
	
	int counter = 0;
	char temp[100];

	while (isdigit(*s) && *s != EOF && *s != '\n')
	{
		test = 1;
		temp[counter++] = *s++;
	}

	if (*s == EOF || *s == '\n')
		return 0;

	int n = 0;

	temp[counter] = '\0';
	counter = 0;

	while (temp[counter])
		n = 10 * n + (temp[counter++] - '0') ;

	n *= sign;
	*num = n;

	return test;
}