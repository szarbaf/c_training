/*
This program reads a line from stdin and store it in a string using pointer arithmetics.
*/

#include <stdio.h>

#define MAX_LENGTH 5


void getline_p(char s[], int max_length);

int main()
{
	char s[100];
	
	printf("Please enter a line of strings : \n");

	getline_p(s, MAX_LENGTH);

	printf("The line you entered was : %s\n", s);

	system("PAUSE");

	return 0;
}

void getline_p(char s[], int max_length)
{
	int c, counter = 0;

	while ((c = getchar()) != EOF && c != '\n' && counter < max_length - 1)
	{
		*s++ = c;
		counter++;
	}

	*s = '\0';
}