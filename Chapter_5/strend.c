/*
This program returns one if string t occurs at the end of string s and zero otherwise.
*/

#include <stdio.h>

int My_strend(char s[], char t[]);

int main()
{
	char s[] = "Shapaly Vivi?", t[] = "Vivi?";

	int checker = My_strend(s, t);

	printf("The result of the test is : %d\n", checker);
	
	system("PAUSE");

	return 0;
}

int My_strend(char s[], char t[])
{
	char *t_first = t;

	while (*++t != '\0')
		;
	--t;

	while (*++s != '\0')
		;
	--s;

	while (t > t_first)
	{
		if (*t-- != *s--)
			return 0;
	}

	return 1;
}