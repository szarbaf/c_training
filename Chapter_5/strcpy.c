/*
This program copies string t( with at most length MAX_LENGTH) to string s.
*/

#include <stdio.h>

#define MAX_LENGTH 5

void My_strcpy(char s[], char t[], int max_length); 

int main()
{
	char s[100], t[] = "Shapalia! Syraby.";

	My_strcpy(s, t, MAX_LENGTH);

	printf("The resulting string is : %s\n", s);

	system("PAUSE");
	
	return 0;
}

void My_strcpy(char s[], char t[], int max_length)
{
	int length_checker = 0;

	while (length_checker++ < max_length && (*s++ = *t++))
		;

	*s = '\0';
}