/*
This program concatenates string t( with at most length MAX_LENGTH) to the end of string s using pointer arithmetics.
*/

#include <stdio.h>

#define MAX_LENGTH 5

void My_strcat(char s[], char t[], int max_length);

int main()
{
	char s[100] = "Man shapal ", t[100] = "Nistam!";

	My_strcat(s, t, MAX_LENGTH);

	printf("The resulting concatenated string is : %s\n", s);

	system("PAUSE");

	return 0;
}

void My_strcat(char s[], char t[], int max_length)
{
	int length_checker = 0;
	
	while (*(++s) != '\0')
		;

	while (*t != '\0' && length_checker++ < max_length)
		*s++ = *t++;

	*s = '\0';
}