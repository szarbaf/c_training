/*
This program cross referencer where it shows different words of a file with the associated line numbers they appear on.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define MAXWORD 100
#define MAXLINE 1000

struct tnode{
	char *word;
	int count;
	int line_num[MAXLINE];
	struct tnode *left;
	struct tnode *right;
};

int getword(char *word, int lim, FILE *file);
struct tnode *addtree(struct tnode *root, char *word, int n, int line_n);
void print_tree(struct tnode *root);

int main(int argc, char *argv[])
{
	int n = atoi(argv[1]);
	FILE *file = fopen("test_var_name.c", "r");
	char word[100], line[500];
	int checker = 1, line_n = 1;
	struct tnode *root = NULL;
	
	while(checker != EOF)
	{
		while(1)
		{
			checker = getword(word, MAXWORD, file);
			if (checker == EOF)
				break;
			else if (checker == '\n' )
			{
				if (strcmp(word, "The") * strcmp(word, "and") * strcmp(word, "or") != 0)
					root = addtree(root, word, n, line_n);
				break;
			}
			
			if (strcmp(word, "The") * strcmp(word, "and") * strcmp(word, "or") != 0)
				root = addtree(root, word, n, line_n);
				
		}
		
		if (checker == '\n')
			line_n++;
	}
	
	print_tree(root);
	
	return 0;
}


int getword(char *word, int lim, FILE *file)
{

	char *w = word;
	int c;
	
	while( isspace(c = fgetc(file)) )
		;
	
	if (c == EOF)
		return EOF;
	else if (c == '\n')
		return '\n';
	else
		*w++ = c;
	
	if ( isalnum(c) || c == '_' || c == '#')
		{
			while ( lim-- > 0 && ( isalnum((c = fgetc(file))) || c == '_') )
				*w++ = c;
			if (c == '\n')
				return '\n';
		}
	else if (c == '\'' || c == '"')
		{
			while (lim-- > 0)
			{
				if ( (*w = fgetc(file)) == '\\')
					*++w = fgetc(file);
				else if (*w == c)
				{
					*w++;
					break;
				}
				else if (*w == EOF)
					break;
				else if (*w == '\n')
				{
					ungetc('\n', file);
					break;
				}
				w++;
			}
		}
		else if (c == '/')
		{
			int d;
			if ( (c = fgetc(file)) == '*')
			{
				*w++ = '/';
				*w++ = '*';
				while (lim-- > 0)
				{
					if ( (c = fgetc(file)) != '*' )
						*w++ = c;
					else if (c == '*')
					{
						if ((d = fgetc(file)) == '/')
						{
							*w++ = '*';
							*w++ = '/';
							break;
						}
						else
						{
							*w++ = '*';
							ungetc(c, file);					
						}
					}
					else
						break;
				}
			}
			else
			{
			*w++ = '/';
			ungetc(c, file);
			}
		}
	
	*w = '\0';
	return word[0];
}

struct tnode *addtree(struct tnode *root, char *word, int n, int line_n)
{
	
	if (root == NULL)
	{
		root = calloc(1, sizeof(struct tnode));
		root->word = calloc(1, MAXWORD);
		strncpy(root->word, word, strlen(word) + 1);
		root->count = 1;
		root->line_num[0] = line_n;
		root->left = NULL;
		root->right = NULL;
	}
	else if (strncmp(word, root->word, n) == 0)
	{
		root->line_num[root->count] = line_n;
		root->count++;
	}
	else if (strncmp(word, root->word, n) < 0)
		root->left = addtree(root->left, word, n, line_n);
	else
		root->right = addtree(root->right, word, n, line_n);
		
	return root;
	
}


void print_tree(struct tnode *root)
{
	if (root != NULL)
	{
		print_tree(root->left);
		
		
		printf("%s\tcount : %d\tline numbers : ", root->word, root->count);
		
		for (int i = 0; i < root->count; i++)
			printf("%d ", root->line_num[i]);
		printf("\n");
		
		
		print_tree(root->right);
			
	}
}
