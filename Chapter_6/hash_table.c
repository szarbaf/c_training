/*

   This file imitates the preprocessor of C, it first reads "#define name def" lines and then replaces all instances of "name" with "def". For keeping record of saved names and corresponding definitions, it uses a hashtable.

*/



#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define MAX_LINE 299
#define MAX_WORD 50
#define MAX_HASH 100

enum FLAG {
	ISFILE,
	ISSTRING
};

struct hashtab
{
	char name[MAX_WORD];
	char def[MAX_WORD];

	struct hashtab *next;
};


int my_getline(char *line, int max_line, FILE *in);
int def_check(char *line, char def[], char name[], int max_word);
void install(struct hashtab *hash[], char def[], char name[], int max_word);
int hash_calc(char def[], int max_word);
struct hashtab* look_up(struct hashtab *hash_p, char def[], int max_word);
int hash_check(struct hashtab *hash_p, char def[], char **name, int max_word);
int my_getword(char word[], int max_word, void **in, enum FLAG f);
int spit(void **in, enum FLAG f);
void swallow (void **in, enum FLAG f, char c);
int check_valid_char(char c);
int check_divide_char (char c);
void undefine(struct hashtab *hash[], char def[], int max_word);



int main()
{
	char *in_name = "cross_ref.c";
	char out_name[MAX_WORD];
	strncpy(out_name, "hashed_", MAX_WORD);
	strncat(out_name, in_name, MAX_WORD);

	FILE *in = fopen(in_name, "r");
	FILE *out = fopen(out_name,"w");

	char *line = calloc(MAX_LINE, sizeof(char));
	char def[MAX_WORD];
	char *name = calloc(MAX_WORD, sizeof(char));

	struct hashtab *hash[MAX_HASH] = { NULL };

	int count = 0;

	//Reading definitions
	while ( my_getline(line, MAX_LINE, in) != EOF )

		if ( def_check(line, def, name, MAX_WORD) )
			install(hash, def, name, MAX_WORD);	




	//Replacing definitions
	fclose(in);
	in = fopen(in_name, "r");


	int hash_val;
	char *line_p;

	undefine(hash, "MAXWORD", MAX_WORD);
	while ( my_getline(line, MAX_LINE, in) != EOF )
	{
		line_p = line;
		my_getword(def, MAX_WORD, (void **)(&line_p), ISSTRING);

		if (strncmp(def, "#define", MAX_WORD) != 0)
		{
			do{

				//Check whether the word matches any of the definitions and if it does, name is populated.

				hash_val = hash_calc(def, MAX_WORD);	
				int test = hash_check(hash[hash_val], def, &name, MAX_WORD);

				if ( test == 1 )
					fputs(name, out);
				else
					fputs(def, out);
			}while( my_getword(def, MAX_WORD, (void **)(&line_p), ISSTRING) );
			

		}

	}

	return 0;

}

int my_getline(char *line, int max_line, FILE *in)
{
	int counter = 0;
	char c;

	
	while(counter < max_line && (c = fgetc(in)) != EOF && c!= '\n')
	{

		if (c == '\r')
			c = '\n';
		line[counter++] = c;
	}

	line[counter] = '\0';


	if (c == EOF)
		return EOF;
	else
		return 1;
}

int def_check(char *line, char def[], char name[], int max_word)
{
	my_getword(def, max_word, (void **)(&line), ISSTRING);
	int flag = 0;

	if ( !strncmp(def, "#define", max_word) )
	{
		//Skipping divide characters
		my_getword(def, max_word, (void **)(&line), ISSTRING);
		my_getword(def, max_word, (void **)(&line), ISSTRING);
		//Skipping divide characters
		my_getword(name, max_word, (void **)(&line), ISSTRING);
		my_getword(name, max_word, (void **)(&line), ISSTRING);

		if( name != NULL && def != NULL )
			flag = 1;

	}

	return flag;	
}


void install(struct hashtab *hash[], char def[], char name[], int max_word)
{
	struct hashtab *hash_p;
	int hash_val = hash_calc(def, max_word);

	if ( !(hash_p = look_up(hash[hash_val], def, max_word)) )
	{
		struct hashtab *hash_new = calloc(1, sizeof(struct hashtab));

		strncpy(hash_new->def, def, max_word);
		strncpy(hash_new->name, name, max_word);

		hash_new->next = hash[hash_val];
		hash[hash_val] = hash_new;

	}
	else
		strncpy(hash_p->name, name, max_word);
}

int hash_calc(char def[], int max_word)
{
	int counter;
	long int hash_val;

	for (hash_val = 0, counter = 0; counter < max_word, def[counter] != '\0'; counter++)
		hash_val = def[counter] + 5 * hash_val;

	return hash_val % MAX_HASH;
}

struct hashtab* look_up(struct hashtab *hash_p, char def[], int max_word)
{
	struct hashtab *np;

	for (np = hash_p; np != NULL; np = np->next)
		if ( !strncmp(np->def, def, max_word) )
			return np;

	return NULL;
}

//This function checks whether def exists in the hashtable and populates name accordingly, otherwise name is set to NULL.

int hash_check(struct hashtab *hash_p, char def[], char **name, int max_word)
{

	struct hashtab *np;
	for (np = hash_p; np != NULL; np = np->next)
		if ( !strncmp(np->def, def, max_word) )
		{
			strncpy(*name, np->name, max_word);
			return 1;
		}
	return 0;
}

int my_getword(char word[], int max_word, void **in, enum FLAG f)
{
	char *w = word;
	int c;

	//Determining what the type of input is using f
	int end_char;
	if (f == ISFILE)
		end_char = EOF;
	else
		end_char = '\0';
	
	
	if ( (c = spit(in, f)) == EOF  || c == '\0')
		return c;

	if ( check_divide_char(c) )
	{
		char ci = c;
		*w++ = ci;
		while(--max_word > 0 && (c = spit(in, f)) == ci )
			*w++ = ci;

	}

	
	else if ( check_valid_char(c) )
	{
		*w++ = c;
		while ( --max_word > 0 && check_valid_char( c = spit(in ,f) ) )
			*w++ = c; 
	}
	
	else if (c == '\'' || c == '"')
	{
		*w++ = c;
		while (max_word-- > 0)
		{
			if ( (*w = spit(in, f)) == '\\')
				*++w = spit(in, f);
			else if (*w == c)
			{
				*++w = '\0';
				return word[0];
			}
			else if (*w == end_char)
				break;
			w++;
		}
	}
	else if (c == '/')
	{
		int d;
		if ( (c = spit(in, f)) == '*')
		{
			*w++ = '/';
			*w++ = '*';
			while ((c = spit(in, f))!= end_char && max_word-- > 0)
			{
				if ( c != '*' )
				{
					printf("%c", c);
					*w++ = c;
				}
				else if (c == '*')
				{
					if ((d = spit(in, f)) == '/')
					{
						*w++ = '*';
						*w++ = '/';
						break;
					}
					else
					{
						*w++ = '*';
						swallow(in, f, c);					
					}
				}
				else
					break;
			}
		}
		else
		{
			*w++ = '/';
		}
	}

	if (max_word-- > 0 && c != EOF && c != '\0')
		swallow(in, f, c);

	*w = '\0';
	return word[0];

}

int spit(void **in, enum FLAG f)
{
	if (f == ISFILE)
		return fgetc((FILE*) *in);

	else if(f == ISSTRING)
	{
		char **in_s = (char **) in;
		if (**in_s != '\0')
			return *(*in_s)++;
		else
			return '\0';
	}		

	else
	{
		printf("spit: Error, invalid flag for the input...\n");
		exit(EXIT_FAILURE);
	}
}

void swallow (void **in, enum FLAG f, char c)
{
	if (c == '\0' || c == EOF)
		return;

	if (f == ISFILE)
		ungetc(c, (FILE*) *in);

	else if(f == ISSTRING)
	{
		char **in_s = (char **) in;
		--(*in_s);
		
	}		

	else
	{
		printf("spit: Error, invalid flag for the input...\n");
		exit(EXIT_FAILURE);
	}
}

int check_valid_char(char c)
{
	//if ( isalnum(c) || c == '_' || c == '#' || c == '<' || c == '>'|| c == '.' || c == '*' || c == '\n')
	if (c != EOF && c!= '\0' && c != '"' && c != '\'' && c != '/' && !check_divide_char(c) )
		return 1;
	else
		return 0;
}

int check_divide_char (char c)
{
	if (c == ' ' || c == '(' || c == ')' || c == '[' || c == ']' || c == ',' || c == '\t' || c == '\n')
		return 1;
	else
		return 0;
}

void undefine(struct hashtab *hash[], char def[], int max_word)
{
	struct hashtab *np, *np_p;
	int hash_val;

	hash_val = hash_calc(def, max_word);
	np_p = np = hash[hash_val];
	
	int counter = 0;
	for ( ; np != NULL; np = np->next, counter++)
	{
		if ( !strncmp(np->def, def, max_word) )
		{
			if (counter != 0)
			{
				np_p->next = np->next;
				free(np);
			}
			else
			{
				hash[hash_val] = NULL;
				free(np);
			}
			return;
		}
		np_p = np;
	}
	
	printf("undefine: The definition %s not found.\n", def);
}

