/*
   The function getword reads a word from the file stream.
   */

#include <stdio.h>
#include <ctype.h>

#define MAXWORD 100

int getword(char *word, int lim, FILE *file);

int main()
{

	char word[100];
	int check = 1;

	while (1)
	{
		check = getword(word, MAXWORD, stdin);
		if (check > 0)
			printf("** %s **\n", word);
		else
			break;
	}

	return 0;
}

int getword(char *word, int lim, FILE *file)
{
	char *w = word;
	int c;

	while( isspace(c = getchar()) )
		;

	if (c != EOF)
		*w++ = c;
	else
		return EOF;

	if ( isalnum(c) || c == '_' || c == '#')
	{
		while ( lim-- > 0 && ( isalnum((c = fgetc(file))) || c == '_') )
			*w++ = c; 
	}
	else if (c == '\'' || c == '"')
	{
		while (lim-- > 0)
		{
			if ( (*w = fgetc(file)) == '\\')
				*++w = fgetc(file);
			else if (*w == c)
			{
				*w++;
				break;
			}
			else if (*w == EOF)
				break;
			w++;
		}
	}
	else if (c == '/')
	{
		int d;
		if ( (c = fgetc(file)) == '*')
		{
			*w++ = '/';
			*w++ = '*';
			while (lim-- > 0)
			{
				if ( (c = fgetc(file)) != '*' )
					*w++ = c;
				else if (c == '*')
				{
					if ((d = fgetc(file)) == '/')
					{
						*w++ = '*';
						*w++ = '/';
						break;
					}
					else
					{
						*w++ = '*';
						ungetc(c, file);					
					}
				}
				else
					break;
			}
		}
		else
		{
			*w++ = '/';
			ungetc(c, file);
		}
	}

	*w = '\0';
	return word[0];
}
