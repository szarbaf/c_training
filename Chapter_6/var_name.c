/*
This program reads a c file and finds the groups of variables that have the same first "n" characters and
the number of such variables in each group.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define MAXWORD 100

struct tnode{
	char *word;
	int count;
	struct tnode *left;
	struct tnode *right;
};

int getword(char *word, int lim, FILE *file);
int check_var(char word[]);
struct tnode *addtree(struct tnode *root, char *word, int n);
void print_tree(struct tnode *root);

int main(int argc, char *argv[])
{
	int n = atoi(argv[1]);
	FILE *file = fopen("test_var_name.c", "r");
	char word[100];
	int checker = 1;
	struct tnode *root = NULL;
	
	while(1)
	{
		
		checker = getword(word, MAXWORD, file);
		if (checker <= 0)
			break;
		
		if ( check_var(word) )
		{
			checker = getword(word, MAXWORD, file);
			if (checker <= 0)
				break;
				
			root = addtree(root, word, n);
				
		}			
	}
	
	print_tree(root);
	
	return 0;
}


int getword(char *word, int lim, FILE *file)
{
	char *w = word;
	int c;
	
	while( isspace(c = fgetc(file)) )
		;
	
	if (c != EOF)
		*w++ = c;
	else
		return EOF;
	
	if ( isalnum(c) || c == '_' || c == '#')
		{
			while ( lim-- > 0 && ( isalnum((c = fgetc(file))) || c == '_') )
				*w++ = c; 
		}
	else if (c == '\'' || c == '"')
		{
			while (lim-- > 0)
			{
				if ( (*w = fgetc(file)) == '\\')
					*++w = fgetc(file);
				else if (*w == c)
				{
					*w++;
					break;
				}
				else if (*w == EOF)
					break;
				w++;
			}
		}
		else if (c == '/')
		{
			int d;
			if ( (c = fgetc(file)) == '*')
			{
				*w++ = '/';
				*w++ = '*';
				while (lim-- > 0)
				{
					if ( (c = fgetc(file)) != '*' )
						*w++ = c;
					else if (c == '*')
					{
						if ((d = fgetc(file)) == '/')
						{
							*w++ = '*';
							*w++ = '/';
							break;
						}
						else
						{
							*w++ = '*';
							ungetc(c, file);					
						}
					}
					else
						break;
				}
			}
			else
			{
			*w++ = '/';
				ungetc(c, file);
			}
		}
	
	*w = '\0';
	return word[0];
}



int check_var(char word[])
{
	
	if (strncmp(word, "int", 3) == 0 || strncmp(word, "float", 5)  == 0 || strncmp(word, "double", 6)  == 0 || strncmp(word, "char", 4)  == 0)
		return 1;
	else
		return 0;
		
}

struct tnode *addtree(struct tnode *root, char *word, int n)
{
	
	if (root == NULL)
	{
		root = calloc(1, sizeof(struct tnode));
		root->word = calloc(1, MAXWORD);
		strncpy(root->word, word, strlen(word) + 1);
		root->count = 1;
		root->left = NULL;
		root->right = NULL;
	}
	else if (strncmp(word, root->word, n) == 0)
		root->count++;
	else if (strncmp(word, root->word, n) < 0)
		root->left = addtree(root->left, word, n);
	else
		root->right = addtree(root->right, word, n);
		
	return root;
	
}


void print_tree(struct tnode *root)
{
	if (root != NULL)
	{
		print_tree(root->left);
		
		printf("%s\tcount == %d\n", root->word, root->count);
		
		print_tree(root->right);
			
	}
}

