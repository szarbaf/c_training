/*
This program cross referencer where it shows different words of a file with the associated line numbers they appear on.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define MAXWORD 100
#define MAXLIST 1000

struct tnode{
	char *word;
	int count;
	struct tnode *left;
	struct tnode *right;
};

int getword(char *word, int lim, FILE *file);
struct tnode *addtree(struct tnode *root, char *word, int n, int *num_words);
void sub(int left, int right, struct tnode *node_list[], int count_list[]);
void populate(struct tnode *root, struct tnode *node_list[], int count_list[]);
void sort_tnode(int low, int high, struct tnode *node_list[], int count_list[]);
void print_node_list(struct tnode *node_list[], int count_list[], int num_words);

int main(int argc, char *argv[])
{
	int n = atoi(argv[1]);
	FILE *file = fopen("test_var_name.c", "r");
	char word[100];
	int checker = 1, num_words = 0;
	struct tnode *root = NULL;
	struct tnode *node_list[MAXLIST];
	int count_list[MAXLIST];
	while(1)
	{
		checker = getword(word, MAXWORD, file);
		if (checker == EOF)
			break;
		root = addtree(root, word, n, &num_words);
	}
	
	populate(root, node_list, count_list);
	sort_tnode(0, num_words-1, node_list, count_list);
	print_node_list(node_list, count_list, num_words);
	
	return 0;
}

int getword(char *word, int lim, FILE *file)
{

	char *w = word;
	int c;
	
	while( isspace(c = fgetc(file)) )
		;
	
	if (c == EOF)
		return EOF;
	else if (c == '\n')
		return '\n';
	else
		*w++ = c;
	
	if ( isalnum(c) || c == '_' || c == '#')
		{
			while ( lim-- > 0 && ( isalnum((c = fgetc(file))) || c == '_') )
				*w++ = c;
			if (c == '\n')
				return '\n';
		}
	else if (c == '\'' || c == '"')
		{
			while (lim-- > 0)
			{
				if ( (*w = fgetc(file)) == '\\')
					*++w = fgetc(file);
				else if (*w == c)
				{
					w++;
					break;
				}
				else if (*w == EOF)
					break;
				else if (*w == '\n')
				{
					ungetc('\n', file);
					break;
				}
				w++;
			}
		}
		else if (c == '/')
		{
			int d;
			if ( (c = fgetc(file)) == '*')
			{
				*w++ = '/';
				*w++ = '*';
				while (lim-- > 0)
				{
					if ( (c = fgetc(file)) != '*' )
						*w++ = c;
					else if (c == '*')
					{
						if ((d = fgetc(file)) == '/')
						{
							*w++ = '*';
							*w++ = '/';
							break;
						}
						else
						{
							*w++ = '*';
							ungetc(c, file);					
						}
					}
					else
						break;
				}
			}
			else
			{
			*w++ = '/';
			ungetc(c, file);
			}
		}
	
	*w = '\0';
	return word[0];
}

struct tnode *addtree(struct tnode *root, char *word, int n, int *num_words)
{
	
	if (root == NULL)
	{
		root = calloc(1, sizeof(struct tnode));
		root->word = calloc(1, MAXWORD);
		strncpy(root->word, word, strlen(word) + 1);
		root->count = 1;
		(*num_words)++;		
		root->left = NULL;
		root->right = NULL;
	}
	else if (strncmp(word, root->word, n) == 0)
		root->count++;
	else if (strncmp(word, root->word, n) < 0)
		root->left = addtree(root->left, word, n, num_words);
	else
		root->right = addtree(root->right, word, n, num_words);
		
	return root;
	
}

void populate(struct tnode *root, struct tnode *node_list[], int count_list[])
{
	static int index = 0;

	if (root == NULL)
		return;

	node_list[index] = root;
	count_list[index] = root->count;
	index++;
	
	populate(root->left, node_list, count_list);
	populate(root->right, node_list, count_list);
}


void sort_tnode(int low, int high, struct tnode *node_list[], int count_list[])
{

	int mid = (high + low) / 2;
	int current = low + 1;

	if (low >= high)
		return;
	
	sub(low, mid, node_list, count_list);

	for (int i = low + 1; i < high ; i++)
		if ( count_list[i] > count_list[low] )
			sub(current++, i, node_list, count_list);

	sub(low, current - 1, node_list, count_list);
	
	sort_tnode(low, current - 2, node_list, count_list);
	sort_tnode(current, high, node_list, count_list);

}

void sub(int left, int right, struct tnode *node_list[], int count_list[])
{
	int tmp;
	struct tnode *tmp_node;

	tmp = count_list[left];
	count_list[left] = count_list[right];
	count_list[right] = tmp;

	tmp_node = node_list[left];
	node_list[left] = node_list[right];
	node_list[right] = tmp_node;	
}

void print_node_list(struct tnode *node_list[], int count_list[], int num_words)
{
	for (int i = 0; i < num_words; i++)
		printf("%s\tcount : %d\n", node_list[i]->word, count_list[i]);
}


